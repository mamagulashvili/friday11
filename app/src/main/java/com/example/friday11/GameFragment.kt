package com.example.friday11

import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.friday11.databinding.GameFragmentBinding

class GameFragment : Fragment() {

    private var _binding: GameFragmentBinding? = null
    private val binding get() = _binding!!

    private val gameAdapter: GameAdapter by lazy { GameAdapter() }
    private val buttonList = mutableListOf<ButtonModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = GameFragmentBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        setData()
        binding.rvGame.apply {
            layoutManager = GridLayoutManager(requireContext(), 3)
            adapter = gameAdapter
        }
        gameAdapter.setData(buttonList)
        gameAdapter.onButtonClick = {

            gameAdapter.swapItems(it,gameAdapter.EMPTY_BTN_POS)
        }
    }
    private fun setData(){
        buttonList.apply {
            for (i in 1..9){
                add(ButtonModel(i))
            }
        }
        buttonList.shuffle()
    }
}