package com.example.friday11

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.friday11.databinding.RowItemBinding

typealias onButtonClick = (position: Int) -> Unit

class GameAdapter : RecyclerView.Adapter<GameAdapter.GameViewHolder>() {
    companion object{
        private const val EMPTY_BTN = 9
    }

    private val itemList = mutableListOf<ButtonModel>()
    lateinit var onButtonClick: onButtonClick
    var EMPTY_BTN_POS = 0
    inner class GameViewHolder(val binding: RowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var model: ButtonModel

        fun onBind() {
            model = itemList[adapterPosition]
            if (model.id == EMPTY_BTN){
                binding.root.text = " "
                EMPTY_BTN_POS = adapterPosition
            }else{
                binding.root.text = model.id.toString()
            }
            if(model.id == adapterPosition + 1 ){
                binding.root.setBackgroundColor(Color.RED)
            }
            binding.root.setOnClickListener {
                if (model.id != EMPTY_BTN ){
                    onButtonClick.invoke(adapterPosition)
                }
            }

        }
    }

    fun swapItems(itemAIndex: Int, itemBIndex: Int) {
        val itemA = itemList[itemAIndex]
        val itemB = itemList[itemBIndex]
        itemList[itemAIndex] = itemB
        itemList[itemBIndex] = itemA
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameViewHolder =
        GameViewHolder(
            RowItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: GameViewHolder, position: Int) = holder.onBind()

    override fun getItemCount(): Int = 9

    fun setData(new: MutableList<ButtonModel>) {
        this.itemList.clear()
        this.itemList.addAll(new)
        notifyDataSetChanged()
    }
}